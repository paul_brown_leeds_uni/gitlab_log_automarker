# Automarker for Gitlab Repo Logs

Developed for COMP1921.
Assumes marks out of 5. Feel free to fork and adapt.

## How to install

We have a single dependency to provide the GUI: ["Gooey"](https://github.com/chriskiehl/Gooey).

Recommended way to run is using ["Pipenv"](https://pipenv.pypa.io/en/latest/),
if you don't have Pipenv already installed, you can pip install it:

```bash
pip install pipenv
```

To use Pipenv to install the dependency do:

```bash
pipenv install
```

(Note: takes suprisingly long to install gooey)

To run the application you can then do:

```
pipenv run automarker
```

## How it works

**IMPORTANT**
The script assumes Gitlab SSH URLS of the format:

```
git@gitlab.com:<group>/<project_id>.git
```

The group is typically used to identify a cohort (e.g. `comp1921_2021`),
the project id is often a student id to identify the student (e.g. `sc16pb`).

The GUI will ask you to set the group. It will also ask you to provide a CSV
file with the project ids. This file can be a single column CSV, but if it has
many columns it **assumes the first column contains the project id**.

The program will sequentially clone projects and check their logs. It does this
into a `tmp` directory if possible, otherwise it will do so in the current
directory.
