#!/usr/bin/env python
"""
Gitlab Log Automarker
"""
__author__ = "Paul Brown"
__version__ = "1.0.0"
__contact__ = "sc16pb@leeds.ac.uk"

from argparse import FileType
import csv
from datetime import date
import os
from pathlib import Path
import shutil
from statistics import mean
import subprocess as sp
import tempfile

from gooey import GooeyParser, Gooey

WORKING_DIRECTORY = Path(tempfile.gettempdir()).joinpath("automarker_tmp_data")
# Clear, fresh working dir
if WORKING_DIRECTORY.exists():
    os.rmdir(str(WORKING_DIRECTORY))
os.mkdir(str(WORKING_DIRECTORY))


class Marker:
    """Marker orchestrates the whole marking process"""

    def __init__(self, config):
        self.student_ids = []
        self.config = config
        self._read_in_student_ids(self.config.SID_FILE)

    def _read_in_student_ids(self, file_name):
        with open(file_name) as sid_file:
            reader = csv.reader(sid_file)
            self.student_ids = [row[0] for row in reader]

    def mark_all(self):
        """Mark all projects found in input file"""
        for sid in self.student_ids:
            self.check_log(sid)

    def check_log(self, student_id):
        """Marking process for a single project"""
        print(f"Checking {student_id}", end=", ")
        repo_path = WORKING_DIRECTORY.joinpath(student_id)
        if self.clone_student_repo(student_id, repo_path):
            os.chdir(repo_path)
            score = self.mark_log(student_id)
            os.chdir(WORKING_DIRECTORY)
            shutil.rmtree(repo_path)
            print(f"Scored: {score}")
            with open(self.config.OUT_FILE, "a") as scores:
                print(f"{student_id}, {score}", file=scores)
        else:
            print(f"Failed to clone {student_id}")
            score = "N/A"
            with open(self.config.OUT_FILE, "a") as scores:
                print(f"{student_id}, {score}", file=scores)

    def clone_student_repo(self, student_id, repo_path):
        """Download the project repo to tmp dir"""
        command = [
            "git",
            "clone",
            self.config.CLONE.format(sid=student_id),
            str(repo_path),
        ]
        try:
            sp.run(command, check=True)
        except sp.CalledProcessError:
            return False
        return True

    def mark_log(self, student_id):
        """Mark the git log of the project"""
        gitlog = sp.run(
            ["git", "--no-pager", "log", "--pretty=%as|%s"],
            capture_output=True,
            text=True,
        )
        log = Log(
            map(lambda r: Commit(r[:10], r[11:]), gitlog.stdout.splitlines()),
            self.config,
        )
        return Scorer(log, self.config).score


class Commit:
    """A single commit in the log"""

    def __init__(self, commitdate: str, message: str):
        self.date = date.fromisoformat(commitdate)
        self.message = message

    def between(self, d1, d2):
        return d1 <= self.date <= d2

    @property
    def message_length(self):
        return len(self.message.split(" "))


class Log:
    """A collection of commits between the set date and due date"""

    def __init__(self, commits: list, config):
        self.commits = list(
            filter(
                lambda c: c.between(config.set_date, config.due_date), commits
            )
        )

    @property
    def length(self):
        return len(self.commits)

    @property
    def earliest(self):
        return self.commits[-1]

    @property
    def latest(self):
        return self.commits[0]

    @property
    def duration(self):
        return self.latest.date - self.earliest.date


class Scorer:
    """Scores a git log"""

    def __init__(self, log, config):
        self.log = log
        self.config = config

    @property
    def used_version_control(self) -> bool:
        """If the log length is greater than 1, they get a mark for using
        version control"""
        return self.log.length > 1

    @property
    def good_number_of_commits(self) -> bool:
        """A mark for a good number of commit"""
        return self.log.length >= self.config.good_number_of_commits

    @property
    def good_dev_duration(self) -> bool:
        """A mark for working over a period of time
        (rather than a 1 day rush)"""
        if not self.used_version_control:
            return False
        return self.log.duration.days >= self.config.good_dev_duration

    @property
    def started_early(self) -> bool:
        """A mark for not leaving it to the last minute"""
        if not self.used_version_control:
            return False
        time_to_start = self.log.earliest.date - self.config.set_date
        return time_to_start.days <= self.config.good_start_duration

    @property
    def good_length_messages(self) -> bool:
        """A mark as a proxy to informative commit messages"""
        if not self.used_version_control:
            return False
        avg_message_length = mean(
            [commit.message_length for commit in self.log.commits]
        )
        return avg_message_length >= self.config.good_length_messages

    @property
    def score(self) -> int:
        """Add up the marks

        (In Python we can sum booleans as True=1, False=0)
        """
        return sum(
            [
                self.used_version_control,
                self.good_number_of_commits,
                self.good_dev_duration,
                self.started_early,
                self.good_length_messages,
            ]
        )


@Gooey
def main():
    """
    Build the GUI and execute
    """
    parser = GooeyParser(description="Mark Project Vesion Control Use")
    parser.add_argument(
        "project_ids_file",
        type=FileType("r"),
        help=(
            "CSV with Project (Student) IDs to mark "
            "in first (and maybe only) column"
        ),
        widget="FileChooser",
    )
    parser.add_argument(
        "out_file",
        type=FileType("a"),
        help="File to append scores too",
    )
    parser.add_argument(
        "set_date",
        help="Date the Coursework was set in ISO format (YYYY-MM-DD)",
        widget="DateChooser",
    )
    parser.add_argument(
        "due_date",
        help="Date the Coursework was due in ISO format (YYYY-MM-DD)",
        widget="DateChooser",
    )
    parser.add_argument(
        "gitlab_group",
        help="The group part for Gitlab URL (http://gitlab.com/<group>/<sid>)",
    )
    parser.add_argument(
        "email",
        help="Email address of Gitlab user for cloning student repos",
    )
    parser.add_argument(
        "password",
        help="Email address of Gitlab user for cloning student repos",
        widget="PasswordField",
    )

    parser.add_argument(
        "--good_number_of_commits",
        help="The minimum number of commits to accept as good",
        default=7,
        type=int,
        widget="IntegerField",
    )
    parser.add_argument(
        "--good_dev_duration",
        help=(
            "The minimum number of days from "
            "first to last commit to accept as good"
        ),
        default=7,
        type=int,
        widget="IntegerField",
    )
    parser.add_argument(
        "--good_start_duration",
        help=(
            "The minimum number of days from "
            "set date to the first commit to accept as good"
        ),
        default=7,
        type=int,
        widget="IntegerField",
    )
    parser.add_argument(
        "--good_length_messages",
        help="The minimum average length of a message to accept as good",
        default=3,
        type=int,
        widget="IntegerField",
    )

    args = parser.parse_args()

    # Little manipulation of the args so they can be used as config
    args.email = args.email.replace("@", "%40")
    args.CLONE = f"https://{args.email}:{args.password}@gitlab.com/"
    args.CLONE += f"{args.gitlab_group}/"
    args.CLONE += "{sid}.git"
    args.SID_FILE = args.project_ids_file.name
    args.OUT_FILE = args.out_file.name
    args.set_date = date.fromisoformat(args.set_date)
    args.due_date = date.fromisoformat(args.due_date)

    # Clear out file
    with open(args.OUT_FILE, "w") as out_file:
        print("", file=out_file)

    # Run the marker
    marker = Marker(args)
    marker.mark_all()


if __name__ == "__main__":
    main()
